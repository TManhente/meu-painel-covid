<!--
SPDX-FileCopyrightText: 2021 Thiago M. de C. Marques <thiago_manhente@yahoo.com.br>
SPDX-License-Identifier: CC0-1.0
-->

Meu painel COVID
================

Uma página HTML simples que agrega alguns gráficos do [Our World in Data](https://ourworldindata.org/) sobre a COVID-19.

https://tmanhente.gitlab.io/meu-painel-covid